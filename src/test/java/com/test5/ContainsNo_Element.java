package com.test5;
public class ContainsNo_Element {
	 public static void main(String[] args) {
		
	        String inputString = "Orange";
	
	        char ch = 'r';
	
	        int count = 0;
	
	        for (int i = 0; i < inputString.length(); i++) {
	
	            if (inputString.charAt(i) == ch) {
	                count++;
	            }
	        }
	
	        System.out.println("The character '" + ch + "' found " + count + " times in a string '" + inputString + "'.");
	    }
}
